const jsonwt = require("jsonwebtoken");
const key = require("../setup/db");
const UserService = require("../services/UserService");

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.signup = async (req, res) => {

    const user = {
        email: req.body.email,
        password: req.body.password
    }

    try {
        if (await UserService.userExist(user.email)) {
            res.status(422).send("User already exists...");
            return;
        }
        await UserService.createUser(user);

        res.status(201).send();
    } catch (e) {
        console.log("Error in controller is ", e.message);
        res.status(500).send('Internal Error');
    }
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.login = async (req, res) => {

    const user = {
        email: req.body.email,
        password: req.body.password
    }

    try {
        const userModel = await UserService.getUserByEmail(user.email);
        if (!userModel) {
            res.status(404).send("User not exist");
            return;
        }

        if (!await UserService.isPasswordsEquals(user.password, userModel.password)) {
            res.status(403).send("Wrong password");
            return;
        }

        const payload = {
            id: userModel.id,
            email: userModel.email
        };

        const token = await jsonwt.sign(
            payload,
            key.secret,
            { expiresIn: 3600 },
        );

        res.json({
            success: true,
            token: "Bearer " + token
        });

    } catch (e) {
        console.log("Error in controller is ", e.message);
        res.status(500).send('Internal Error');
    }
}