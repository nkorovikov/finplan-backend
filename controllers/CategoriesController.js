const UserService = require("../services/UserService");
const CategoriesService = require("../services/CategoriesService");

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.show = async (req, res) => {

    const userModel = await UserService.getUserByEmail(req.user.email);
    
    res.json({
        id: req.user.id,
        email: req.user.email,
        categories: userModel.categories
    });
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.store = async (req, res) => {

    await CategoriesService.saveCategories(req.user.email, req.body);

    res.status(200).send();
}