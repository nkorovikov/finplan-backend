const UserService = require("../services/UserService");
const ProfileService = require("../services/ProfileService");

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.show = async (req, res) => {

    const userModel = await UserService.getUserByEmail(req.user.email);

    res.json({
        id: req.user.id,
        email: req.user.email,
        profile: userModel.profile
    });
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.store = async (req, res) => {

    const profile = {
        name: req.body.name,
        dailyBudget: req.body.dailyBudget,
        weeklyBudget: req.body.weeklyBudget,
        monthlyBudget: req.body.monthlyBudget,
        locale: req.body.locale,
    }

    await ProfileService.saveProfile(req.user.email, profile);

    res.status(200).send();
}