const UserService = require("../services/UserService");
const ExpensesService = require("../services/ExpensesService");

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.show = async (req, res) => {

    const userModel = await UserService.getUserByEmail(req.user.email);

    res.json({
        id: req.user.id,
        email: req.user.email,
        expenses: userModel.expenses
    });
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.store = async (req, res) => {

    await ExpensesService.saveExpenses(req.user.email, req.body);

    res.status(200).send();
}