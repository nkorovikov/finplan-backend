const express = require("express");
const router = express.Router();
const AuthController = require('../controllers/AuthController');
const SignUpValidator = require('../validators/SignUpValidator');
const LogInValidator = require('../validators/LogInValidator');

router.post("/signup", SignUpValidator.perform, AuthController.signup);

router.post("/login", LogInValidator.perform, AuthController.login);

module.exports = router;