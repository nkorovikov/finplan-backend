const express = require("express");
const passport = require("passport");
const router = express.Router();
const ProfileController = require('../controllers/ProfileController');
const ProfileValidator = require('../validators/ProfileValidator');

router.get(
    "/profile",
    passport.authenticate("jwt", { session: false }),
    ProfileController.show
);

router.post(
    "/profile",
    passport.authenticate("jwt", { session: false }),
    ProfileValidator.perform,
    ProfileController.store
);

module.exports = router;