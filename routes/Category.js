const express = require("express");
const passport = require("passport");
const router = express.Router();
const CategoriesController = require('../controllers/CategoriesController');
const CategoriesValidator = require('../validators/CategoriesValidator');

router.get(
    "/categories",
    passport.authenticate("jwt", { session: false }),
    CategoriesController.show
);

router.post(
    "/categories",
    passport.authenticate("jwt", { session: false }),
    CategoriesValidator.perform,
    CategoriesController.store
);

module.exports = router;