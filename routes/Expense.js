const express = require("express");
const passport = require("passport");
const router = express.Router();
const ExpensesController = require('../controllers/ExpensesController');
const ExpensesValidator = require('../validators/ExpensesValidator');

router.get(
    "/expenses",
    passport.authenticate("jwt", { session: false }),
    ExpensesController.show
);

router.post(
    "/expenses",
    passport.authenticate("jwt", { session: false }),
    ExpensesValidator.perform,
    ExpensesController.store
);

module.exports = router;