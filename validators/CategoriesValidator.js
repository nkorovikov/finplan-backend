// @TODO REFACTOR

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
const sanitize = async (req, res, next) => {

    if (!Array.isArray(req.body)) {
        req.body = Array(req.body)
    }

    req.body = req.body.map((category) => {
        return {
            id: Number(category.id),
            name: String(category.name),
            type: Number(category.type),
            icon: String(category.icon)
        }
    });
}

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
exports.perform = async (req, res, next) => {
    await sanitize(req, res, next)

    next();
}