// @TODO REFACTOR

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
const sanitize = async (req, res, next) => {

    req.body.name = String(req.body.name);

    if (req.body.dailyBudget === "" || req.body.dailyBudget === undefined) {
        req.body.dailyBudget = null
    } else {
        req.body.dailyBudget = Number(req.body.dailyBudget);
    }

    if (req.body.weeklyBudget === "" || req.body.weeklyBudget === undefined) {
        req.body.weeklyBudget = null
    } else {
        req.body.weeklyBudget = Number(req.body.weeklyBudget);
    }

    if (req.body.monthlyBudget === "" || req.body.monthlyBudget === undefined) {
        req.body.monthlyBudget = null;
    } else {
        req.body.monthlyBudget = Number(req.body.monthlyBudget);
    }

    req.body.locale = String(req.body.locale);
}

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
const validate = async (req, res, next) => {
    const errors = {
        name: [],
        dailyBudget: [],
        weeklyBudget: [],
        monthlyBudget: [],
        locale: [],
    };

    if (typeof req.body.name !== 'string' || req.body.name === '[object Object]') {
        errors.name.push('name must be a string');
    }

    if (req.body.name.length > 100) {
        errors.name.push('name length must be a lower than 100 symbols');
    }

    if (req.body.name.length === 0) {
        errors.name.push('name must be a not empty');
    }

    if (isNaN(req.body.dailyBudget)) {
        errors.dailyBudget.push('dailyBudget must be a number');
    }

    if (isNaN(req.body.weeklyBudget)) {
        errors.weeklyBudget.push('weeklyBudget must be a number');
    }

    if (isNaN(req.body.monthlyBudget)) {
        errors.monthlyBudget.push('monthlyBudget must be a number');
    }

    if (typeof req.body.locale !== 'string' || req.body.locale === '[object Object]') {
        errors.locale.push('Locale must be a string');
    }

    if (!['en', 'ru'].includes(req.body.locale)) {
        errors.locale.push('Locale has a wrong value');
    }

    return errors;

}

/**
 * 
 * @param {object} obj 
 */
const removeEmpty = (obj) => {
    for (var key in obj) {
        if (obj[key].length === 0)
            delete obj[key];
    }
    return obj;
}


/**
 * 
 * @param {object} obj 
 */
const isEmpty = (obj) => {
    for (var key in obj) {
        if (obj[key].length > 0)
            return false;
    }
    return true;
}

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
exports.perform = async (req, res, next) => {
    await sanitize(req, res, next)
    errors = await validate(req, res, next);

    errors = removeEmpty(errors);

    if (!isEmpty(errors)) {
        res.status(422).json(errors);
        return;
    }

    next();
}