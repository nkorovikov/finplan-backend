// @TODO REFACTOR

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
const sanitize = async (req, res, next) => {

    if (!Array.isArray(req.body)) {
        req.body = Array(req.body)
    }

    req.body = req.body.map((expense) => {
        return {
            id: Number(expense.id),
            sum: Number(expense.sum),
            categoryId: Number(expense.categoryId),
            createdAt: Number(expense.createdAt)
        }
    });
}

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
exports.perform = async (req, res, next) => {
    await sanitize(req, res, next)

    next();
}