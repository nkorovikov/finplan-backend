// @TODO REFACTOR

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
const sanitize = async (req, res, next) => {

    req.body.name = String(req.body.email);
    req.body.password = String(req.body.password);
}

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
const validate = async (req, res, next) => {
    const errors = {
        email: [],
        password: [],
    };

    if (typeof req.body.email !== 'string' || req.body.email === '[object Object]') {
        errors.email.push('email must be a string');
    }

    if (req.body.email.length > 100) {
        errors.email.push('email length must be a lower than 100 symbols');
    }

    if (req.body.email.length === 0) {
        errors.email.push('email must be a not empty');
    }

    if (!validateEmail(req.body.email)) {
        errors.email.push('email must be a email');
    }

    if (typeof req.body.password !== 'string' || req.body.password === '[object Object]') {
        errors.password.push('password must be a string');
    }

    if (req.body.password.length > 100) {
        errors.password.push('password length must be a lower than 100 symbols');
    }

    if (req.body.password.length === 0) {
        errors.password.push('password must be a not empty');
    }

    return errors;

}

/**
 * 
 * @param {string} email 
 */
const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/**
 * 
 * @param {object} obj 
 */
const removeEmpty = (obj) => {
    for (var key in obj) {
        if (obj[key].length === 0)
            delete obj[key];
    }
    return obj;
}


/**
 * 
 * @param {object} obj 
 */
const isEmpty = (obj) => {
    for (var key in obj) {
        if (obj[key].length > 0)
            return false;
    }
    return true;
}

/**
 * 
 * @param {object} req 
 * @param {object} res 
 * @param {object} next 
 */
exports.perform = async (req, res, next) => {
    await sanitize(req, res, next)
    errors = await validate(req, res, next);

    errors = removeEmpty(errors);

    if (!isEmpty(errors)) {
        res.status(422).json(errors);
        return;
    }

    next();
}