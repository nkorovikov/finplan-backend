const UserService = require("../services/UserService");

/**
 * 
 * @param {string} email 
 * @param {object} profile 
 */
exports.saveProfile = async (email, profile) => {
    const user = await UserService.getUserByEmail(email);
    user.profile = profile;
    user.save();
}