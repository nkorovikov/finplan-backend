const UserService = require("../services/UserService");

/**
 * 
 * @param {string} email 
 * @param {Array} expenses 
 */
exports.saveExpenses = async (email, expenses) => {
    const user = await UserService.getUserByEmail(email);
    user.expenses = expenses;
    user.save();
}