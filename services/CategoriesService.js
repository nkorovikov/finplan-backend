const UserService = require("../services/UserService");

/**
 * 
 * @param {string} email 
 * @param {Array} categories 
 */
exports.saveCategories = async (email, categories) => {
    const user = await UserService.getUserByEmail(email);
    user.categories = categories;
    user.save();
}