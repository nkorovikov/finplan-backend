const bcrypt = require("bcrypt");
const User = require("../models/User");
const saltRounds = 10;

/**
 * 
 * @param {string} email 
 */
exports.userExist = async (email) => {
    const profile = await User.findOne({ email });

    return !!profile;
}

/**
 * 
 * @param {string} email 
 */
exports.getUserByEmail = async (email) => {
    return await User.findOne({ email });
}

/**
 * 
 * @param {object} user 
 */
exports.createUser = async (user) => {
    const hash = await bcrypt.hash(user.password, saltRounds);

    new User({
        email: user.email,
        password: hash
    }).save();
}

/**
 * 
 * @param {string} transmittedPassword 
 * @param {string} modelPassword 
 * @returns {boolean}
 */
exports.isPasswordsEquals = async (transmittedPassword, modelPassword) => {
    return await bcrypt.compare(transmittedPassword, modelPassword);
}