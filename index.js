require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const bodyparser = require("body-parser");
const passport = require("passport");
const db = require("./setup/db").url;
const app = express();

const port = process.env.PORT || 3000;

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

mongoose
    .connect(db)
    .then(() => {
        console.log("Database is connected");
    })
    .catch(err => {
        console.log("Error is ", err.message);
    });

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,Authorization');
    next();
});

const auth = require("./routes/Auth");
const profile = require("./routes/Profile");
const expenses = require("./routes/Expense");
const categories = require("./routes/Category");

app.use("/api", auth);
app.use("/api", profile);
app.use("/api", expenses);
app.use("/api", categories);

app.use(passport.initialize());

require("./strategies/jsonwtStrategy")(passport);

app.get("/", (req, res) => {
    res.status(200);
});

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});