const mongoose = require("mongoose");

const CategorySchema = mongoose.Schema({
    id: {
        type: Number,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    type: {
        type: Number,
        require: true
    },
    icon: {
        type: String,
        require: true
    }
});

exports = CategorySchema;