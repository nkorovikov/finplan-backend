const mongoose = require("mongoose");

const ExpenseSchema = mongoose.Schema({
    id: {
        type: Number,
        require: true
    },
    sum: {
        type: Number,
        require: true
    },
    categoryId: {
        type: Number,
        require: true
    },
    createdAt: {
        type: Number,
        require: true
    }
});

exports = ExpenseSchema;