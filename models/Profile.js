const mongoose = require("mongoose");

const ProfileSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    dailyBudget: {
        type: Number,
        default: null
    },
    weeklyBudget: {
        type: Number,
        default: null
    },
    monthlyBudget: {
        type: Number,
        default: null
    },
    locale: {
        type: String,
        require: true
    }
});

exports = ProfileSchema;