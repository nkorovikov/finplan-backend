const mongoose = require("mongoose");
const Profile = require("./Profile");
const Expense = require("./Expense");
const Category = require("./Category");

const UserSchema = mongoose.Schema({
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    profile: {
        type: Profile,
        default: {}
    },
    expenses: {
        type: [Expense],
        default: []
    },
    categories: {
        type: [Category],
        default: []
    }
});

module.exports = User = mongoose.model("User", UserSchema);